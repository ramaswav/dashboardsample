import React from 'react';
import logo from "../assets/adminapp.png";
import { MDBListGroup, MDBListGroupItem, MDBIcon } from 'mdbreact';
import { NavLink } from 'react-router-dom';

const TopNavigation = () => {
    return (
        <div className="sidebar-fixed position-fixed">
            <a href="#!" className="logo-wrapper waves-effect">
                <img alt="QuiltAI" className="img-fluid" src={logo}/>
            </a>
            <MDBListGroup className="list-group-flush">
                <NavLink exact={true} to="/" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="chart-pie" className="mr-3"/>
                        Entities
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/demograph" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="candy-cane" className="mr-3"/>
                        DemoGraph
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/influencer" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="diagnoses" className="mr-3"/>
                        Influencer
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/objectdetection" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="dragon" className="mr-3"/>
                        Object Detection
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/wordcloud" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="file-word" className="mr-3"/>
                        WordCloud
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/sources" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="percent" className="mr-3"/>
                        SourcePercentage
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/profile" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="table" className="mr-3"/>
                        Articles
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/tables" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="user" className="mr-3"/>
                        Polls
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/maps" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="map" className="mr-3"/>
                        Maps
                    </MDBListGroupItem>
                </NavLink>
                <NavLink to="/404" activeClassName="activeClass">
                    <MDBListGroupItem>
                        <MDBIcon icon="exclamation" className="mr-3"/>
                        404
                    </MDBListGroupItem>
                </NavLink>
            </MDBListGroup>
        </div>
    );
}

export default TopNavigation;
