import React from 'react';
import { MDBRow } from 'mdbreact';
import BreadcrumSection from './sections/BreadcrumSection';
import WordClouds from './sections/WordClouds';

const WordCloud =  () => {
  return (
    <React.Fragment>
      {/*  <BreadcrumSection /> */}
      <WordClouds />
    </React.Fragment>
  )
}

export default WordCloud;
