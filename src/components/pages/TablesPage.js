import React from 'react'
import { MDBRow, MDBCol, MDBView, MDBCard, MDBCardBody, MDBTable, MDBTableHead, MDBTableBody } from 'mdbreact';
import AdminCardSection2 from './sections/AdminCardSection2';
import Polls from './sections/Polls';

const TablesPage =  () => {

  return (
    <>
      <Polls/>
      <MDBRow>
      <MDBCol md="12">
        <AdminCardSection2 />
      </MDBCol>
    </MDBRow>
    </>
  )
}

export default TablesPage;
