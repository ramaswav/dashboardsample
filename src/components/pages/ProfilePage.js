import React from 'react'
import { MDBCard, MDBCol, MDBRow, MDBView, MDBMask, MDBCardImage, MDBCardBody, MDBCardTitle, MDBCardText, MDBCardFooter, MDBBtn, MDBIcon } from 'mdbreact';
import src1 from '../../assets/img-1.jpg';

const ProfilePage =  () => {
  return (
    <React.Fragment>
        <MDBRow className="justify-content-center">
        <MDBCol sm="12" md="6" lg="3" className="mb-5">
            <MDBCard>
                <MDBCardImage className="img-fluid" src={src1} />
                <MDBCardBody>
                    <MDBCardTitle className="text-center mb-2 font-bold">What is your stance on abortion?</MDBCardTitle>
                    <MDBCardText>
                        <strong className="mb-2">Abortion:</strong>
                        Pro-life, but allow in cases of rape, incest, or danger to the mother or child
                    </MDBCardText>
                    <div className="row justify-content-end pr-1">
                        <MDBBtn size="sm" outline color="primary">More...</MDBBtn>
                    </div>
                </MDBCardBody>
            </MDBCard>
        </MDBCol>
        <MDBCol md="6" lg="9">
        <section className="text-center pb-3">
          <MDBRow className="d-flex justify-content-center">
            <MDBCol lg="6" xl="5" className="mb-3">
              <MDBCard className="d-flex mb-5">
                <MDBView>
                  <img src="https://mdbootstrap.com/img/Mockups/Horizontal/6-col/pro-profile-page.jpg" alt="Project" className="img-fluid"/>
                  <MDBMask overlay="white-slight"/>
                </MDBView>
                <MDBCardBody>
                  <MDBCardTitle className="font-bold mb-3">
                    <strong>Do you support the legalization of same sex marriage?</strong>
                  </MDBCardTitle>
                  <MDBCardText>Take the government out of marriage and instead make it a religious decision.</MDBCardText>
                </MDBCardBody>

              </MDBCard>
            </MDBCol>
            <MDBCol lg="6" xl="5" className="mb-3">
              <MDBCard className="d-flex mb-5">
                <MDBView>
                  <img src="https://mdbootstrap.com/img/Mockups/Horizontal/6-col/pro-signup.jpg" alt="Project" className="img-fluid"/>
                  <MDBMask overlay="white-slight"/>
                </MDBView>
                <MDBCardBody>
                  <MDBCardTitle className="font-bold mb-3">
                    <strong>Should gender identity be added to anti-discrimination laws?</strong>
                  </MDBCardTitle>
                  <MDBCardText>Some quick example text to build on the card title and make up the bulk of the card's content.</MDBCardText>
                </MDBCardBody>

              </MDBCard>
            </MDBCol>
          </MDBRow>
          <MDBRow className="d-flex justify-content-center">
            <MDBCol lg="6" xl="5" className="mb-3">
              <MDBCard className="d-flex mb-5">
                <MDBView>
                  <img src="https://mdbootstrap.com/img/Mockups/Horizontal/6-col/pro-profile-page.jpg" alt="Project" className="img-fluid"/>
                  <MDBMask overlay="white-slight"/>
                </MDBView>
                <MDBCardBody>
                  <MDBCardTitle className="font-bold mb-3">
                    <strong>Should businesses be required to have women on their board of directors?</strong>
                  </MDBCardTitle>
                  <MDBCardText>Yes, and the government should do more to require diversity in the workplace</MDBCardText>
                </MDBCardBody>

              </MDBCard>
            </MDBCol>
            <MDBCol lg="6" xl="5" className="mb-3">
              <MDBCard className="d-flex mb-5">
                <view-wrapper>
                  <img src="https://mdbootstrap.com/img/Mockups/Horizontal/6-col/pro-signup.jpg" alt="Project" className="img-fluid"/>
                  <MDBMask overlay="white-slight"/>
                </view-wrapper>
                <MDBCardBody>
                  <MDBCardTitle className="font-bold mb-3">
                    <strong>Should women be allowed to wear a Niqāb, or face veil, to civic ceremonies?</strong>
                  </MDBCardTitle>
                  <MDBCardText>No, seemes like only people who would want to are ugly, subjugated, brainwashed, uneducated women, or terrorists</MDBCardText>
                </MDBCardBody>

              </MDBCard>
            </MDBCol>
          </MDBRow>
        </section>
      </MDBCol>
    </MDBRow>
    </React.Fragment>
  );
}

export default ProfilePage;
