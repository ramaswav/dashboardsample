import React from 'react';
import { MDBRow } from 'mdbreact';
import AdminCardSection1 from './sections/AdminCardSection1';
import TableSection from './sections/TableSection';
import BreadcrumSection from './sections/BreadcrumSection';
import ENTITIES from './sections/ENTITIES';
import ChartSection2 from './sections/ChartSection2';
import MapSection from './sections/MapSection';
import ModalSection from './sections/ModalSection';

const DashboardPage =  () => {
  return (
    <React.Fragment>
      {/*  <BreadcrumSection /> */}
      <ENTITIES />
      {/* <ChartSection2 /> */}
    </React.Fragment>
  )
}

export default DashboardPage;
