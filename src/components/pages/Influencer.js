import React from 'react';
import { MDBRow } from 'mdbreact';
import BreadcrumSection from './sections/BreadcrumSection';
import InfluencerS from './sections/Influencer';

const Influencer =  () => {
  return (
    <React.Fragment>
      {/*  <BreadcrumSection /> */}
      <InfluencerS />
    </React.Fragment>
  )
}

export default Influencer;
