import React from 'react';
import { MDBRow } from 'mdbreact';
import BreadcrumSection from './sections/BreadcrumSection';
import SourcePercentageS from './sections/SourcePercentage';

const SourcePercentage =  () => {
  return (
    <React.Fragment>
      {/*  <BreadcrumSection /> */}
      <SourcePercentageS />
    </React.Fragment>
  )
}

export default SourcePercentage;
