import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBCardHeader, MDBRow, MDBListGroup, MDBListGroupItem, MDBBadge, MDBIcon } from 'mdbreact';
import { Doughnut } from 'react-chartjs-2';
import csv from 'csvtojson';
import request from 'request';
import randomcolor from 'randomcolor';

class SourcePercentage extends Component {

  constructor(props){
    super(props);
    this.state = {
      SourcePercentage: {}
    }
  }

  getData(){
    let SourcePercentageObject={
        labels: [],
        datasets: []
    };
    csv()
    .fromStream(request.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/SOURCE_PERCENTAGE.csv'))
    .then((SourcePercentageData)=>{
        //console.log(jsonObj);
        let datasetObj= {
            data: [],
            backgroundColor: [],
            hoverBackgroundColor: []
        };
        SourcePercentageData.forEach(function(row,index) {
          SourcePercentageObject.labels.push(row.analytics_data__name);
          datasetObj.data.push(row.analytics_data__value);
          datasetObj.backgroundColor.push(randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }));
          datasetObj.hoverBackgroundColor.push(randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }));
          SourcePercentageObject.datasets.push(datasetObj);
        });
        return SourcePercentageObject;
    })
    .then((SourcePercentageObject)=>{
      console.log(SourcePercentageObject);
      this.setState({SourcePercentage: SourcePercentageObject});
    });
  }

  componentDidMount(){
    this.getData();
    setInterval(function() {
      this.getData();
    }.bind(this), 10000);
  }

    render(){
        return (
          <MDBCol md="12" lg="8" className="mb-5">
              <MDBCard className="mb-4">
              <MDBCardHeader>Source Percentage</MDBCardHeader>
              <MDBCardBody >
                  <Doughnut data={this.state.SourcePercentage}  height={800}  width={800} options={{responsive: true }} />
              </MDBCardBody>
              </MDBCard>
          </MDBCol>
        )
    }
}

export default SourcePercentage;
