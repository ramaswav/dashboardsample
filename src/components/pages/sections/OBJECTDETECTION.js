import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBCardHeader, MDBRow, MDBListGroup, MDBListGroupItem, MDBBadge, MDBIcon } from 'mdbreact';
import { Bar, Pie } from 'react-chartjs-2';
import csv from 'csvtojson';
import request from 'request';
import randomcolor from 'randomcolor';

class OBJECTDETECTION extends Component {

  constructor(props){
    super(props);
    this.state = {
      OBJECTDETECTIONData: {}
    }
  }

  getData(){
    let OBJECTDETECTIONDataObject={
        labels: [],
        datasets: [{
            data: [],
            backgroundColor: [],
            hoverBackgroundColor: []
        }
        ]
    };
    csv()
    .fromStream(request.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/OBJECT_DETECTION.csv'))
    .then((OBJECTDETECTIONData)=>{
        //console.log(jsonObj);
        let datasetObj= {
            data: [],
            backgroundColor: [],
            hoverBackgroundColor: []
        };
        OBJECTDETECTIONData.forEach(function(row,index) {
          OBJECTDETECTIONDataObject.labels.push(row.analytics_data__name);
          OBJECTDETECTIONDataObject.datasets[0].data.push(row.analytics_data__count);
          OBJECTDETECTIONDataObject.datasets[0].backgroundColor.push(randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }));
          OBJECTDETECTIONDataObject.datasets[0].hoverBackgroundColor.push(randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }));
        });
        return OBJECTDETECTIONDataObject;
    })
    .then((OBJECTDETECTIONDataObject)=>{
      console.log(OBJECTDETECTIONDataObject);
      this.setState({OBJECTDETECTIONData: OBJECTDETECTIONDataObject});
    });
  }

  componentDidMount(){
    this.getData();
    setInterval(function() {
      this.getData();
    }.bind(this), 10000);
  }

    render(){

        return (
          <MDBRow className="mb-12">
              <MDBCol md="12" className="mb-5">
                  <MDBCard className="mb-6">
                      <MDBCardHeader>OBJECTDETECTION</MDBCardHeader>
                      <MDBCardBody>
                          <Pie data={this.state.OBJECTDETECTIONData} height={300} options={{responsive: true}} />
                      </MDBCardBody>
                  </MDBCard>
              </MDBCol>
          </MDBRow>
        )
    }
}

export default OBJECTDETECTION;
