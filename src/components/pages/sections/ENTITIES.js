import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBCardHeader, MDBRow, MDBListGroup, MDBListGroupItem, MDBBadge, MDBIcon } from 'mdbreact';
import { Bar, Pie } from 'react-chartjs-2';
import csv from 'csvtojson';
import request from 'request';
import randomcolor from 'randomcolor';

class ENTITIES extends Component {

  constructor(props){
    super(props);
    this.state = {
      entityData: {},
      maxEntites: {}
    }
  }

  getData(){
    let entityDataObject={};
    csv()
    .fromStream(request.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/ENTITIES.csv'))
    .then((entityData)=>{
        //console.log(jsonObj);
        entityData.forEach(function(row,index) {
          //console.log(row);
          //console.log(index);
          let tempObj= {};
          let tempArray= [];
          tempArray.push(row.analytics_data__count);
          tempObj[row.analytics_data__text]=tempArray;
          if ( entityDataObject[row.analytics_data__type] === undefined) {
            entityDataObject[row.analytics_data__type] = [];
            entityDataObject[row.analytics_data__type].push(tempObj)
          }
          else {
            entityDataObject[row.analytics_data__type].push(tempObj);
          }
        });
        return entityDataObject;
    })
    .then((entityDataObject)=>{
      let Entities = [];
      let maxEntities={
        labels:['MAX ENTITIES'],
        datasets:[]
      };
      Object.entries(entityDataObject).forEach(([key, val, index]) => {
          let mapObj={
            labels:[],
            datasets:[]
          };
          mapObj.labels.push(key);
          let maxdata=0;
          let datasetObjMax={
            label:'',
            data: [],
            backgroundColor: randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }),
            borderWidth: 1
          };

          Object.entries(val).forEach(([i,v]) => {
            let datasetObj={
              label:'',
              data: [],
              backgroundColor: randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }),
              borderWidth: 1
            };
            let KeyBe=Object.keys(val[i])[0];
            datasetObj.label=KeyBe;
            datasetObj.data=Object.values(val[i])[0];
            if ( parseInt(datasetObj.data[0]) > parseInt(maxdata) ) {
              console.log(datasetObj.data[0]);
              maxdata=datasetObj.data[0];
            }
            mapObj.datasets.push(datasetObj);
          });     // the value of the current key.
          datasetObjMax.data[0]=maxdata;
          datasetObjMax.label=key;
          Entities.push(mapObj);
          maxEntities.datasets.push(datasetObjMax);
      });
      this.setState({entityData: Entities, maxEntites: maxEntities});
    });
  }

  componentDidMount(){
    this.getData();
    setInterval(function() {
      this.getData();
    }.bind(this), 10000);
  }

    render(){
        // const dataBarPeople = {
        //     labels: ['Person'],
        //     datasets: [
        //     {
        //         label: 'James',
        //         data: [230],
        //         backgroundColor: 'rgba(245, 74, 85, 0.5)',
        //         borderWidth: 1
        //     }, {
        //         label: 'LeBron James',
        //         data: [76],
        //         backgroundColor: 'rgba(90, 173, 246, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Kyrie Irving',
        //         data: [49],
        //         backgroundColor: 'rgba(245, 195, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Stephen Curry',
        //         data: [46],
        //         backgroundColor: 'rgba(247, 292, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Anthony Davis',
        //         data: [44],
        //         backgroundColor: 'rgba(248, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Kevin Durant',
        //         data: [44],
        //         backgroundColor: 'rgba(242, 92, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Giannis Antetokounmpo',
        //         data: [38],
        //         backgroundColor: 'rgba(249, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Doncic',
        //         data: [36],
        //         backgroundColor: 'rgba(225, 167, 50, 0.5)',
        //         borderWidth: 1
        //     }
        //     ]
        // };
        // const dataBarOrganization = {
        //     labels: ['Organization'],
        //     datasets: [
        //     {
        //         label: 'NBA',
        //         data: [230],
        //         backgroundColor: 'rgba(245, 74, 85, 0.5)',
        //         borderWidth: 1
        //     }, {
        //         label: 'Lakers',
        //         data: [76],
        //         backgroundColor: 'rgba(90, 173, 246, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Los Angeles Lakers',
        //         data: [49],
        //         backgroundColor: 'rgba(245, 195, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Minnesota Timberwolves',
        //         data: [46],
        //         backgroundColor: 'rgba(247, 292, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Timberwolves',
        //         data: [44],
        //         backgroundColor: 'rgba(248, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Milwaukee Bucks',
        //         data: [44],
        //         backgroundColor: 'rgba(242, 92, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Cavaliers',
        //         data: [38],
        //         backgroundColor: 'rgba(249, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Boston Celtics',
        //         data: [36],
        //         backgroundColor: 'rgba(225, 167, 50, 0.5)',
        //         borderWidth: 1
        //     }
        //     ]
        // };
        // const dataBarLocation = {
        //     labels: ['Location'],
        //     datasets: [
        //     {
        //         label: 'NBA',
        //         data: [230],
        //         backgroundColor: 'rgba(245, 74, 85, 0.5)',
        //         borderWidth: 1
        //     }, {
        //         label: 'Lakers',
        //         data: [76],
        //         backgroundColor: 'rgba(90, 173, 246, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Los Angeles Lakers',
        //         data: [49],
        //         backgroundColor: 'rgba(245, 195, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Minnesota Timberwolves',
        //         data: [46],
        //         backgroundColor: 'rgba(247, 292, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Timberwolves',
        //         data: [44],
        //         backgroundColor: 'rgba(248, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Milwaukee Bucks',
        //         data: [44],
        //         backgroundColor: 'rgba(242, 92, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Cavaliers',
        //         data: [38],
        //         backgroundColor: 'rgba(249, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Boston Celtics',
        //         data: [36],
        //         backgroundColor: 'rgba(225, 167, 50, 0.5)',
        //         borderWidth: 1
        //     }
        //     ]
        // };
        // const dataBarHandle = {
        //     labels: ['Organization'],
        //     datasets: [
        //     {
        //         label: 'NBA',
        //         data: [230],
        //         backgroundColor: 'rgba(245, 74, 85, 0.5)',
        //         borderWidth: 1
        //     }, {
        //         label: 'Lakers',
        //         data: [76],
        //         backgroundColor: 'rgba(90, 173, 246, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Los Angeles Lakers',
        //         data: [49],
        //         backgroundColor: 'rgba(245, 195, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Minnesota Timberwolves',
        //         data: [46],
        //         backgroundColor: 'rgba(247, 292, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Timberwolves',
        //         data: [44],
        //         backgroundColor: 'rgba(248, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Milwaukee Bucks',
        //         data: [44],
        //         backgroundColor: 'rgba(242, 92, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Cavaliers',
        //         data: [38],
        //         backgroundColor: 'rgba(249, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Boston Celtics',
        //         data: [36],
        //         backgroundColor: 'rgba(225, 167, 50, 0.5)',
        //         borderWidth: 1
        //     }
        //     ]
        // };
        // const dataBarHashTag = {
        //     labels: ['Organization'],
        //     datasets: [
        //     {
        //         label: 'NBA',
        //         data: [230],
        //         backgroundColor: 'rgba(245, 74, 85, 0.5)',
        //         borderWidth: 1
        //     }, {
        //         label: 'Lakers',
        //         data: [76],
        //         backgroundColor: 'rgba(90, 173, 246, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Los Angeles Lakers',
        //         data: [49],
        //         backgroundColor: 'rgba(245, 195, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Minnesota Timberwolves',
        //         data: [46],
        //         backgroundColor: 'rgba(247, 292, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Timberwolves',
        //         data: [44],
        //         backgroundColor: 'rgba(248, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Milwaukee Bucks',
        //         data: [44],
        //         backgroundColor: 'rgba(242, 92, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Cavaliers',
        //         data: [38],
        //         backgroundColor: 'rgba(249, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Boston Celtics',
        //         data: [36],
        //         backgroundColor: 'rgba(225, 167, 50, 0.5)',
        //         borderWidth: 1
        //     }
        //     ]
        // };
        // const dataBarEmoji = {
        //     labels: ['Organization'],
        //     datasets: [
        //     {
        //         label: 'NBA',
        //         data: [230],
        //         backgroundColor: 'rgba(245, 74, 85, 0.5)',
        //         borderWidth: 1
        //     }, {
        //         label: 'Lakers',
        //         data: [76],
        //         backgroundColor: 'rgba(90, 173, 246, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Los Angeles Lakers',
        //         data: [49],
        //         backgroundColor: 'rgba(245, 195, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Minnesota Timberwolves',
        //         data: [46],
        //         backgroundColor: 'rgba(247, 292, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Timberwolves',
        //         data: [44],
        //         backgroundColor: 'rgba(248, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Milwaukee Bucks',
        //         data: [44],
        //         backgroundColor: 'rgba(242, 92, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Cavaliers',
        //         data: [38],
        //         backgroundColor: 'rgba(249, 142, 50, 0.5)',
        //         borderWidth: 1
        //     },
        //     {
        //         label: 'Boston Celtics',
        //         data: [36],
        //         backgroundColor: 'rgba(225, 167, 50, 0.5)',
        //         borderWidth: 1
        //     }
        //     ]
        // };

        const barChartOptions = {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
            xAxes: [{
                barPercentage: 1,
                gridLines: {
                display: true,
                color: 'rgba(0, 0, 0, 0.1)'
                }
            }],
            yAxes: [{
                gridLines: {
                display: true,
                color: 'rgba(0, 0, 0, 0.1)'
                },
                ticks: {
                beginAtZero: true
                }
            }]
            }
        }
        // const dataPie = {
        //     labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul'],
        //     datasets: [
        //     {
        //         data: [300, 50, 100, 40, 120, 24, 52],
        //         backgroundColor: ['#F7464A', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360', '#ac64ad'],
        //         hoverBackgroundColor: ['#FF5A5E', '#5AD3D1', '#FFC870', '#A8B3C5', '#616774', '#da92db']
        //     }
        //     ]
        // }
        return (
            <MDBRow className="mb-12">
                <MDBCol md="12"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>Max Entities</MDBCardHeader>
                        <MDBCardBody>
                            <Bar data={this.state.maxEntites} height={500} options={barChartOptions} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>People</MDBCardHeader>
                        <MDBCardBody>
                            <Bar data={this.state.entityData[0]} height={500} options={barChartOptions} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>Organization</MDBCardHeader>
                        <MDBCardBody>
                            <Bar data={this.state.entityData[1]} height={500} options={barChartOptions} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>Location</MDBCardHeader>
                        <MDBCardBody>
                            <Bar data={this.state.entityData[2]} height={500} options={barChartOptions} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>Handle</MDBCardHeader>
                        <MDBCardBody>
                            <Bar data={this.state.entityData[3]} height={500} options={barChartOptions} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>Hashtag</MDBCardHeader>
                        <MDBCardBody>
                            <Bar data={this.state.entityData[4]} height={500} options={barChartOptions} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                <MDBCol md="6"className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>Emoji</MDBCardHeader>
                        <MDBCardBody>
                            <Bar data={this.state.entityData[5]} height={500} options={barChartOptions} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                { /*<MDBCol md="4" className="mb-4">
                    <MDBCard className="mb-4">
                        <MDBCardHeader>Pie chart</MDBCardHeader>
                        <MDBCardBody>
                            <Pie data={dataPie} height={300} options={{responsive: true}} />
                        </MDBCardBody>
                    </MDBCard>
                    <MDBCard className="mb-4">
                        <MDBCardBody>
                            <MDBListGroup className="list-group-flush">
                                <MDBListGroupItem>
                                    Sales
                                    <MDBBadge color="success-color" pill className="float-right">
                                        22%
                                        <MDBIcon icon="arrow-up" className="ml-1"/>
                                    </MDBBadge>
                                </MDBListGroupItem>
                                <MDBListGroupItem>
                                    Traffic
                                    <MDBBadge color="danger-color" pill className="float-right">
                                        5%
                                        <MDBIcon icon="arrow-down" className="ml-1"/>
                                    </MDBBadge>
                                </MDBListGroupItem>
                                <MDBListGroupItem>
                                    Orders
                                    <MDBBadge color="primary-color" pill className="float-right">
                                        14
                                    </MDBBadge>
                                </MDBListGroupItem>
                                <MDBListGroupItem>
                                    Issues
                                    <MDBBadge color="primary-color" pill className="float-right">
                                        123
                                    </MDBBadge>
                                </MDBListGroupItem>
                                <MDBListGroupItem>
                                    Messages
                                    <MDBBadge color="primary-color" pill className="float-right">
                                        8
                                    </MDBBadge>
                                </MDBListGroupItem>
                            </MDBListGroup>
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
                */ }
            </MDBRow>
        )
    }
}

export default ENTITIES;
