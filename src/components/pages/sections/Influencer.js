import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBCardHeader, MDBRow, MDBListGroup, MDBListGroupItem, MDBBadge, MDBIcon } from 'mdbreact';
import { Bar, Pie } from 'react-chartjs-2';
import csv from 'csvtojson';
import request from 'request';
import randomcolor from 'randomcolor';

class Influencer extends Component {

  constructor(props){
    super(props);
    this.state = {
      influencerData: {}
    }
  }

  getData(){
    let InfluencerDataObject={
        labels: ['Influencer'],
        datasets: []
    };
    let Influencers={};
    csv()
    .fromStream(request.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/INFLUENCER.csv'))
    .then((InfluencerData)=>{
        //console.log(InfluencerData);
        InfluencerData.forEach(function(row,index) {
          //console.log(row);
          //console.log(parseFloat(row.analytics_data__reach));
          if ( Influencers[row.analytics_data__type] == undefined ) {
            Influencers[row.analytics_data__type]=0;
          }
          Influencers[row.analytics_data__type] = parseFloat(Influencers[row.analytics_data__type]) + parseFloat(row.analytics_data__reach);
        });
        return Influencers;
    })
    .then((Influencers)=>{
      Object.keys(Influencers).forEach(function(val){
        let mapObj={
            label: '',
            data: [],
            backgroundColor: randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }),
            borderWidth: 1
        };
        mapObj.label=val;
        mapObj.data[0]= Influencers[val];
        InfluencerDataObject.datasets.push(mapObj);
      });
      //console.log(InfluencerDataObject);
      this.setState({influencerData: InfluencerDataObject});
    });
  }

  componentDidMount(){
    this.getData();
    setInterval(function() {
      this.getData();
    }.bind(this), 10000);
  }

    render(){

      const barChartOptions = {
          responsive: true,
          maintainAspectRatio: false,
          scales: {
          xAxes: [{
              barPercentage: 1,
              gridLines: {
              display: true,
              color: 'rgba(0, 0, 0, 0.1)'
              }
          }],
          yAxes: [{
              gridLines: {
              display: true,
              color: 'rgba(0, 0, 0, 0.1)'
              },
              ticks: {
              beginAtZero: true
              }
          }]
          }
      }
        return (
            <MDBRow className="mb-12">
              <MDBCol md="8"className="mb-4">
                  <MDBCard className="mb-4">
                      <MDBCardHeader>Influencer</MDBCardHeader>
                      <MDBCardBody>
                          <Bar data={this.state.influencerData} height={500} options={barChartOptions} />
                      </MDBCardBody>
                  </MDBCard>
              </MDBCol>
            </MDBRow>
        )
    }
}

export default Influencer;
