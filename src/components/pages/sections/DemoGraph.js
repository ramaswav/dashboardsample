import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBCardHeader, MDBRow, MDBListGroup, MDBListGroupItem, MDBBadge, MDBIcon } from 'mdbreact';
import { Bar, Pie } from 'react-chartjs-2';
import csv from 'csvtojson';
import request from 'request';
import randomcolor from 'randomcolor';

class DemoGraph extends Component {

  constructor(props){
    super(props);
    this.state = {
      demographData: {}
    }
  }

  getData(){
    let demographDataObject={
        labels: [],
        datasets: []
    };
    csv()
    .fromStream(request.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/DEMOGRAPHICS.csv'))
    .then((demographData)=>{
        //console.log(jsonObj);
        let datasetObj= {
            data: [],
            backgroundColor: [],
            hoverBackgroundColor: []
        };
        demographData.forEach(function(row,index) {
          demographDataObject.labels.push(row.analytics_data__name)
          datasetObj.data.push(row.analytics_data__count);
          datasetObj.backgroundColor.push(randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }));
          datasetObj.hoverBackgroundColor.push(randomcolor({luminosity: 'dark',format: 'rgba', alpha: 0.5 }));
          demographDataObject.datasets.push(datasetObj);
        });
        return demographDataObject;
    })
    .then((demographDataObject)=>{
      console.log(demographDataObject);
      this.setState({demographData: demographDataObject});
    });
  }

  componentDidMount(){
    this.getData();
    setInterval(function() {
      this.getData();
    }.bind(this), 10000);
  }

    render(){

        return (
            <MDBRow className="mb-12">
                <MDBCol md="8" className="mb-5">
                    <MDBCard className="mb-6">
                        <MDBCardHeader>DemoGraph</MDBCardHeader>
                        <MDBCardBody>
                            <Pie data={this.state.demographData} height={300} options={{responsive: true}} />
                        </MDBCardBody>
                    </MDBCard>
                </MDBCol>
            </MDBRow>
        )
    }
}

export default DemoGraph;
