import React, { Component } from 'react';
import { MDBCol, MDBCard, MDBCardBody, MDBCardHeader, MDBRow, MDBListGroup, MDBListGroupItem, MDBBadge, MDBIcon } from 'mdbreact';
import { Bar, Pie } from 'react-chartjs-2';
import csv from 'csvtojson';
import request from 'request';
import randomcolor from 'randomcolor';
import D3Cloud from 'react-d3-cloud';

class WordClouds extends Component {

  constructor(props){
    super(props);
    this.state = {
      keywordsData: {}
    }
  }

  getData(){
    csv()
    .fromStream(request.get(window.location.protocol+ "//"+window.location.hostname+':'+window.location.port +'/dataSet/KEYWORDS.csv'))
    .then( (keywordsData) =>{
        let keywordsDataObject = [];
        keywordsData.forEach(function(row,index) {
          let keywordsDataObj = { text:'', value:''};
          keywordsDataObj.text = row.KEYWORD;
          keywordsDataObj.value = parseInt(row.COUNT);
          keywordsDataObject.push(keywordsDataObj);
        });
        return keywordsDataObject;
    })
    .then((keywordsDataObject)=>{
      console.log(Object.values(keywordsDataObject));
      this.setState({keywordsData: Object.values(keywordsDataObject)});
    });
  }

  componentDidMount(){
    this.getData();
    setInterval(() => {
      this.forceUpdate();
    }, 10000);
  }

    render(){

      const fontSizeMapper = word => Math.log2(word.value) * 2.5;
      const rotate = word => word.value % 360;
      const data = Object.values(this.state.keywordsData);
      return (
       <MDBRow className="mb-12">
           <MDBCol md="6" className="mb-6">
               <MDBCard md="6" className="mb-6">
                   <MDBCardHeader>TagCloud</MDBCardHeader>
                   <MDBCardBody>
                      <D3Cloud width={300} height={400} data={data} fontSizeMapper={fontSizeMapper} />
                   </MDBCardBody>
               </MDBCard>
            </MDBCol>
            <MDBCol md="6" className="mb-5">
               <MDBCard md="6" className="mb-4">
                  <MDBCardHeader>Tag Split</MDBCardHeader>
                   <MDBCardBody>
                       <MDBListGroup className="list-group-flush">
                        {Object.values(this.state.keywordsData).map(obj =>
                          <MDBListGroupItem className="d-flex justify-content-between align-items-center">
                            { obj.text }
                            <MDBBadge className="float-right" color="primary pill">{ obj.value }</MDBBadge>
                          </MDBListGroupItem>
                        )}
                       </MDBListGroup>
                   </MDBCardBody>
               </MDBCard>
           </MDBCol>
       </MDBRow>
        )
    }
}

export default WordClouds;
