import React from 'react';
import { MDBRow } from 'mdbreact';
import BreadcrumSection from './sections/BreadcrumSection';
import OBJECTDETECTIONs from './sections/OBJECTDETECTION';

const OBJECTDETECTION =  () => {
  return (
    <React.Fragment>
      {/*  <BreadcrumSection /> */}
      <OBJECTDETECTIONs />
    </React.Fragment>
  )
}

export default OBJECTDETECTION;
