import React from 'react';
import { MDBRow } from 'mdbreact';
import BreadcrumSection from './sections/BreadcrumSection';
import DemoGraphS from './sections/DemoGraph';

const DemoGraph =  () => {
  return (
    <React.Fragment>
      {/*  <BreadcrumSection /> */}
      <DemoGraphS />
    </React.Fragment>
  )
}

export default DemoGraph;
