import React, { Component } from 'react';
import { MDBNavbar, MDBNavbarBrand, MDBNavbarNav, MDBNavbarToggler, MDBCollapse, MDBNavItem, MDBNavLink, MDBIcon } from 'mdbreact';
import { NavLink } from 'react-router-dom';

class TopNavigation extends Component {
    state = {
        collapse: false
    }

    onClick = () => {
        this.setState({
            collapse: !this.state.collapse,
        });
    }

    toggle = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        });
    }

    render() {
        return (
            <MDBNavbar className="flexible-navbar" light expand="md" scrolling>
                <MDBNavbarBrand href="/">
                    <strong>Quilt Dash</strong>
                </MDBNavbarBrand>
                <MDBNavbarToggler onClick = { this.onClick } />
                <MDBCollapse isOpen = { this.state.collapse } navbar>
                    <MDBNavbarNav left>
                        <MDBNavItem active>
                            <MDBNavLink  to="/"> <MDBIcon icon="chart-pie" className="mr-3"/>Entities</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                          <MDBNavLink  to="/demograph"><MDBIcon icon="candy-cane" className="mr-3"/>DemoGraph</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem >
                          <MDBNavLink to="/influencer"><MDBIcon icon="diagnoses" className="mr-3"/>Influencer</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                          <MDBNavLink to="/objectdetection"><MDBIcon icon="dragon" className="mr-3"/>Object Detect</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                          <MDBNavLink activeClassName="activeClass" to="/wordcloud"><MDBIcon icon="file-word" className="mr-3"/>WordCloud</MDBNavLink>
                        </MDBNavItem>
                        <MDBNavItem>
                          <MDBNavLink to="/sources"><MDBIcon icon="percent" className="mr-3"/>SourcePercentage</MDBNavLink>
                        </MDBNavItem>
                    </MDBNavbarNav>
                    <MDBNavbarNav right>
                        <MDBNavItem>
                            <a className="nav-link navbar-link" rel="noopener noreferrer" target="_blank" href="https://sg.linkedin.com/company/quiltdotai"><MDBIcon fab icon="linkedin" /></a>
                        </MDBNavItem>
                        <MDBNavItem>
                            <a className="nav-link navbar-link" rel="noopener noreferrer" target="_blank" href="https://twitter.com/mdbootstrap"><MDBIcon fab icon="twitter" /></a>
                        </MDBNavItem>
                    </MDBNavbarNav>
                </MDBCollapse>
            </MDBNavbar>
        );
    }
}

export default TopNavigation;
